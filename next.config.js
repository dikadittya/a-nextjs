module.exports = {
    env: {
      DATABASE: {
        MYSQL_HOST: 'localhost',
        MYSQL_PORT: '3306',
        MYSQL_DATABASE: 'spe_academy',
        MYSQL_USER: 'root',
        MYSQL_PASSWORD: ''
      },
      SECRET_KEY: 'DRACxAxYxDx15x09x1994xSPEACADEMY',
      PUBLIC_KEY: 'o7Ytbt9XQLI3PgtebJfKSXKEf0XHU74Y',
      JWT_KEY: 'DRACxAxYxDx15x09x1994xSPEACADEMY',
      CLOUDINARY_URL: "https://api.cloudinary.com/v1_1/dinvux9kv/image/upload",
    }
  }